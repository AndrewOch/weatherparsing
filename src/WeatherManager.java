import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherManager {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите путь к файлу .csv");
        String str = scanner.nextLine();

        File file = new File(str);

        WeatherManager weatherManager = new WeatherManager();
        File result = weatherManager.parseWeather(file);

        System.out.println("Результат сохранён в");
        System.out.println(result.getPath());
    }

    File parseWeather(File file) {
        ArrayList<Weather> weathers = new ArrayList<>();
        File result = new File(file.getParent() + "\\weather_data.txt");

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }

            String line;

            while ((line = bufferedReader.readLine()) != null) {

                Integer year = Integer.valueOf(line.substring(0, 4));
                line = line.substring(4);

                Integer month = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);

                Integer day = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);
                Integer hour = Integer.valueOf(line.substring(1, 3));
                line = line.substring(6);

                Float temperature = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);
                Float relativeHumidity = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Float windSpeed = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Float windDirection = Float.valueOf(line);

                Weather weather = new Weather(year, month, day, hour, temperature, relativeHumidity, windSpeed, windDirection);
                weathers.add(weather);
            }

            bufferedReader.close();
            fileReader.close();

            FileWriter fileWriter = new FileWriter(result);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("Средняя температура    : " + averageTemperature(weathers) + "\n");
            bufferedWriter.write("Средняя влажность      : " + averageHumidity(weathers) + "\n");
            bufferedWriter.write("Средняя скорость ветра : " + averageWindSpeed(weathers) + "\n\n");

            Weather weather = weathers.get(indexOfHighestTemperature(weathers));
            bufferedWriter.write("Самая высокая температура : " + weather.getTemperature() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(indexOfMinimumHumidity(weathers));
            bufferedWriter.write("Самая низкая влажность    : " + weather.getRelativeHumidity() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(indexOfMostPowerfulWind(weathers));
            bufferedWriter.write("Самый сильный ветер       : " + weather.getWindSpeed() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n\n");

            bufferedWriter.write("Самое частое направление ветра : " + findMostCommonWindDirection(weathers));

            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }


    Float averageTemperature(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.temperature;
        }
        return average / (weathers.size());
    }

    Float averageHumidity(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.relativeHumidity;
        }
        return average / (weathers.size());
    }

    Float averageWindSpeed(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.windSpeed;
        }
        return average / (weathers.size());
    }

    int indexOfHighestTemperature(ArrayList<Weather> weathers) {
        int result = 0;
        Float record = weathers.get(0).getTemperature();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getTemperature() > record) {
                record = weathers.get(i).getTemperature();
                result = i;
            }
        }
        return result;
    }

    private int indexOfMinimumHumidity(ArrayList<Weather> weathers) {
        int result = 0;
        Float minimum = weathers.get(0).getRelativeHumidity();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getRelativeHumidity() < minimum) {
                minimum = weathers.get(i).getRelativeHumidity();
                result = i;
            }
        }
        return result;
    }


    private int indexOfMostPowerfulWind(ArrayList<Weather> weathers) {
        int result = 0;
        Float record = weathers.get(0).getWindSpeed();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getWindSpeed() > record) {
                record = weathers.get(i).getWindSpeed();
                result = i;
            }
        }
        return result;
    }

    private String findMostCommonWindDirection(ArrayList<Weather> weathers) {

        int south = 0;
        int north = 0;
        int east = 0;
        int west = 0;

        for (Weather weather : weathers) {
            float dir = weather.getWindDirection();

            float min = dir - 0f;
            if (360f - dir < min) {
                min = 360f - dir;
            }

            min = Math.min(Math.min(min, Math.abs(90f - dir)), Math.min(Math.abs(180f - dir), Math.abs(270f - dir)));

            if (min == Math.min(360f - dir, dir - 0f)) {
                north++;
            }
            if (min == Math.abs(90f - dir)) {
                east++;
            }
            if (min == Math.abs(180f - dir)) {
                south++;
            }
            if (min == Math.abs(270f - dir)) {
                west++;
            }
        }

        if (north > south && north > east && north > west) {
            return "Север";
        }
        if (south > north && south > east && south > west) {
            return "Юг";
        }
        if (west > south && west > east && west > north) {
            return "Запад";
        }
        if (east > south && east > north && east > west) {
            return "Восток";
        }
        return null;
    }

}
