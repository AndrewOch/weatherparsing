public class Weather {

    Integer year;
    Integer month;
    Integer day;
    Integer hour;

    Float temperature;
    Float relativeHumidity;
    Float windSpeed;
    Float windDirection;

    public Weather(Integer year, Integer month, Integer day, Integer hour, Float temperature, Float relativeHumidity, Float windSpeed, Float windDirection) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.temperature = temperature;
        this.relativeHumidity = relativeHumidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", temperature=" + temperature +
                ", relativeHumidity=" + relativeHumidity +
                ", windSpeed=" + windSpeed +
                ", windDirection=" + windDirection +
                '}';
    }

    public Integer getYear() {
        return year;
    }

    public Integer getMonth() {
        return month;
    }

    public Integer getDay() {
        return day;
    }

    public Integer getHour() {
        return hour;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Float getRelativeHumidity() {
        return relativeHumidity;
    }

    public Float getWindSpeed() {
        return windSpeed;
    }

    public Float getWindDirection() {
        return windDirection;
    }
}